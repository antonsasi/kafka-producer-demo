package com.demo.kafka.demo.resource;

import com.demo.kafka.demo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class KafkaPublisherResource {
    private static final String TOPIC_NAME = "messagetopic";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private KafkaTemplate<String, Student> studentKafkaTemplate;


    @RequestMapping(method = RequestMethod.POST, value = "/kafka-publish")
    public void putStudent(@RequestBody Student student) {

    }

    @RequestMapping(method = RequestMethod.GET, value = "/kafka-publish/{message}")
    public void putStringMessage(@PathVariable("message") String message) {
        kafkaTemplate.send(TOPIC_NAME, message);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/kafka-publish/student/{name}/{age}")
    public void putStringMessage(@PathVariable("name") String name, @PathVariable("age") int age) {
        studentKafkaTemplate.send(TOPIC_NAME, new Student(name, age));
    }

}
